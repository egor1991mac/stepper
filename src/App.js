import React from 'react';
import Dialog from './components/dialog';
import Button from '@material-ui/core/Button';
import Select from './components/select';
import Stepper from './components/stepper';
import Datepicker from './components/datepicker';
import ContactForm from './components/contactForm';
import Typography from '@material-ui/core/Typography';
import logo from './logo-mob.png';
import AlertMessage from './components/alert';
import Loader from './components/loader';
import {makeStyles} from "@material-ui/core";
import CssBaseline from '@material-ui/core/CssBaseline';
import FromData from './data/country';

import ToData from './data/curort';
import nanoid from 'nanoid';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {asyncGetFrom, asyncGetTo, getData, asyncSendServer} from "./action";


const useStyles = makeStyles(theme => ({
    flex: {
        display: 'flex',
        flexWrap:'wrap',
        justifyContent:'center',
        margin:'1rem 0 0 0'
    },
    colorButton:{
        border:'1px solid #498926',
        color:'white',
        background: 'radial-gradient(ellipse at center, rgba(171,207,64,1) 0%, rgba(123,176,55,1) 59%, rgba(109,167,52,1) 77%, rgba(90,155,49,1) 100%)',
        "& span":{
            textShadow:'2px 2px 2px rgba(0, 0, 0, 0.4196078431372549)'
        },
        "& svg":{
            fill:'white'
        }
    }

}));

function App({Alert,
                 From,
                 To,
                 getFrom,
                 getTo,
                 Loading,
                 getData,
                 Selected_From,
                 Selected_To,
                 Selected_Days,
                 Selected_People,
                 Selected_Date,
                 Selected_ContactForm,
                 Selected_ContactForm_Text,
                 sendOrder
             },props) {

   


    const [open, setOpen] = React.useState(false);

    React.useEffect(() => {
        if (From.length == 0) {
            getFrom();
        }
        if (To.length == 0) {
            getTo();
        }
    });


    const classes = useStyles();
    const handleOpen = () => {
        setOpen(!open);
    };

    if (!Loading) {
        return (

            <div className="App">
                <CssBaseline/>
                <div className={'ts-stepper'}>
                <Button onClick={handleOpen} className={classes.colorButton}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 80.385 71.892">
                        <g id="Group_24" data-name="Group 24" transform="translate(-46.632 -31.811)">
                            <path id="Path_42" data-name="Path 42" d="M346.526,310.914V304.9h16.282V303.46a34.687,34.687,0,0,0-2.075-11.68,48.1,48.1,0,0,0-12.582-18.653l-1.625-1.393V259.283c0-.7.227-2.472-1.424-2.472-1.56,0-1.457,1.432-1.457,2.472v4.047l-2.025,1.488a40.283,40.283,0,0,0-7.465,7.992c-4.773,6.465-10.67,17.2-13.9,33.512l-.336,1.72h23.724v2.872H303.632l.662,1.911c.557,1.623,5.725,15.878,13.2,15.878h46.354c12.631,0,19.076-15.148,19.345-15.8l.827-1.988Zm11.106-19.134a32.29,32.29,0,0,1,2.265,10.24H349.245a40.8,40.8,0,0,0,1.385-10.24c.008-.5.017-1.008.017-1.536a69.4,69.4,0,0,0-.968-11.412A45.493,45.493,0,0,1,357.632,291.78Zm-11.106-14.119a66.265,66.265,0,0,1,1.241,12.583c0,.528-.01,1.047-.02,1.536a38.4,38.4,0,0,1-1.221,9.259Zm-23.08,27.5a82.6,82.6,0,0,1,10.709-27.143,56.039,56.039,0,0,1,5.678-7.522,90.144,90.144,0,0,0-1.826,18.347q0,1.54.058,2.938c.28,6.82,1.471,11.056,2.4,13.38Zm17.5-13.38q-.056-1.383-.056-2.938a81.837,81.837,0,0,1,2.755-21.738V305.19C342.944,303.825,341.282,299.781,340.946,291.78Zm22.9,34.041H317.491c-3.64,0-7.6-6.743-9.76-12.026H379.54C377.456,317.666,372.077,325.821,363.845,325.821Z" transform="translate(-257 -225)"/>
                        </g>
                    </svg>

                    <span style={{margin:'0.5rem'}}>
                        Быстрый подбор тура
                    </span>
                </Button>
                </div>
                <Dialog
                    open={open}
                    onClose={handleOpen}
                    title={<>
                        <img src={logo} alt="smok-travel"/>
                        <Typography variant={'h6'} color={'primary'}>
                            Быстрой подбор туров
                        </Typography>
                    </>}
                >
                    <Stepper
                        SEND={sendOrder}
                        property = {{Selected_From,
                            Selected_To,
                            Selected_Days,
                            Selected_People,
                            Selected_Date,
                            Selected_ContactForm,
                            Selected_ContactForm_Text
                        }}
                        onClose={handleOpen}
                        //bodyQuery = {body}
                        data={[
                            {
                                valid: Selected_To.valid && Selected_From ? true : false,
                                text: <span>В какую страну <br/> Вы хотели бы отправиться?</span>,
                                component:
                                    <div className={classes.flex}>
                                        <Select
                                            placeholder={'Откуда'}
                                            name={'from'}
                                            options={From.length != 0 && From[0].GetDepartCitiesResult.Data}
                                            GET_DATA={getData('SELECTED_FROM')}
                                            value={Selected_From.length != 0 ? Selected_From.data : ""}
                                            id={nanoid()}
                                        />
                                        <Select
                                            placeholder={'Куда'}
                                            name={'to'}
                                            options={To.length !== 0 && To[0].GetCountriesResult.Data}
                                            GET_DATA={getData('SELECTED_TO')}
                                            value={Selected_To.length != 0 ? Selected_To.data : ""}
                                            id={nanoid()}
                                        />
                                    </div>

                            },
                            {
                                valid: Selected_Date.valid ? true : false,
                                text: <span>Какая дата вылета Вам <br/> будет удобна?</span>,
                                component: <Datepicker
                                    GET_DATA={getData('SELECTED_DATE')}
                                    value={Selected_Date.length != 0 ? Selected_Date.data : undefined}
                                />
                            }, {
                                valid: Selected_Days.valid ? true : false,
                                text: <span>На сколько дней <br/> Вы планируете отдых?</span>,
                                component: <Select
                                    type={'type_array'}
                                    placeholder={'Дней'}
                                    name={'days'}
                                    GET_DATA={getData('SELECTED_DAY')}
                                    value={Selected_Days.length != 0 ? Selected_Days.data : ""}
                                    id={nanoid()}
                                    options={[
                                        {
                                            Id: 1,
                                            Name: '3 дня',
                                        },
                                        {
                                            Id: 2,
                                            Name: '4 дня',
                                        },
                                        {
                                            Id: 3,
                                            Name: '5 дней',
                                        },
                                        {
                                            Id: 4,
                                            Name: '6 дней'
                                        },
                                        {
                                            Id: 5,
                                            Name: '7 дней'
                                        },
                                        {
                                            Id: 6,
                                            Name: '8 дней'
                                        },
                                        {
                                            Id: 7,
                                            Name: '9 дней'
                                        },
                                        {
                                            Id: 8,
                                            Name: '10 дней'
                                        },
                                        {
                                            Id: 9,
                                            Name: '11 дней'
                                        },
                                        {
                                            Id: 10,
                                            Name: '12 дней'
                                        },
                                        {
                                            Id: 11,
                                            Name: '13 дней'
                                        },
                                        {
                                            Id: 12,
                                            Name: '14 дней'
                                        },
                                        {
                                            Id: 12,
                                            Name: 'на 3 недели'
                                        }
                                    ]}
                                />
                            },
                            {
                                valid: Selected_People.valid ? true : false,
                                text: <span>Сколько человек <br/> едет в тур?</span>,
                                component: <Select
                                    type={'type_array'}
                                    placeholder={'Туристов'}
                                    name={'tourist'}
                                    GET_DATA={getData('SELECTED_PEOPLE')}
                                    value={Selected_People.length == 0 ? "" : Selected_People.data}
                                    id={nanoid()}
                                    options={[
                                        {
                                            Id: '1',
                                            Name: '1 взрослый'
                                        },
                                        {
                                            Id: 2,
                                            Name: '1 взрослый 1 ребенок'
                                        },
                                        {
                                            Id: 3,
                                            Name: '1 взрослый 2 ребенка'
                                        },{
                                            Id: 4,
                                            Name: '2 взрослых 1 ребенок'
                                        },
                                        {
                                            Id: 5,
                                            Name: '2 взрослый 2 ребенка'
                                        },
                                        {
                                            Id: 6,
                                            Name: '3 взрослых 1 ребенок'
                                        },{
                                            Id: 7,
                                            Name: '3 взрослых 2 ребенка'
                                        },{
                                            Id: 8,
                                            Name: '3 взрослых 1 ребенок'
                                        },{
                                            Id: 8,
                                            Name: '3 взрослых 2 ребенка'
                                        },
                                    ]}
                                />
                            },
                            {
                                valid: Selected_ContactForm.valid ? true : false,
                                text: <span>Расскажите <br/> о себе поподробней!</span>,
                                component: <ContactForm
                                    GET_DATA={getData("SELECTED_CONTACT_FORM")}
                                    GET_DATA_TEXT={getData}
                                    value={Selected_ContactForm.length > 0 ? Selected_ContactForm[0] : undefined}
                                />
                            }
                        ]}
                    />
                </Dialog>
                {
                    Alert.length > 0 ?
                        <AlertMessage type = {Alert[0].type} message = {Alert[0].text} GET_DATA={getData("ALERT")}/>
                        : null
                }

            </div>

        );
    } else {
        return <>
        <CssBaseline/>
        <Loader/></>
    }

}

const mapStateToProps = state => ({...state});
const mapDispatchToProps = (dispatch) => ({
    getFrom: bindActionCreators(asyncGetFrom, dispatch),
    getTo: bindActionCreators(asyncGetTo, dispatch),
    getData: bindActionCreators(getData, dispatch),
    sendOrder: bindActionCreators(asyncSendServer, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
